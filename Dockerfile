##According to README.md in iottalk_server_1.0...
 
#Use an official Ubuntu & Python runtime as a parent image
FROM ubuntu:16.04

# Set the working directory to /iottalk_container
WORKDIR /iottalk_container

# Copy the current directory contents into the container at /app
ADD . /iottalk_container

# Install any needed packages specified in requirements.txt
 #1) Update system packages 
RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get -y autoremove
 #2) Install dependent system packages 
RUN apt-get install -y net-tools
RUN apt-get install -y sudo
RUN apt-get -y install screen python3 python3-pip libsqlite3-dev libssl-dev openssl
#RUN apt-get -y install mailutils
  # for MusicBox
RUN apt-get -y install nodejs
RUN apt-get -y install npm       
 #3) Enter to directory where your IoTtalk server located in
 #4) Install and update dependent python3 packages
RUN pip3 install --upgrade pip
RUN pip3 install setuptools
RUN pip3 install -r requirements.txt
RUN pip3 install --upgrade requests

# Make specific ports available to the world outside this container
EXPOSE 80 5566 7788 8000 9999

# Define environment variable
ENV NAME IoTtalk

# Run app.py when the container launches
CMD ["./setup/startup.sh"]
