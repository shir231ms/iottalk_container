import time
import requests
import csmapi

while True:
    try:
        profile = {
            'd_name': 'Controller',
            'dm_name': 'Remote_control',
            'u_name': 'yb',
            'is_sim': False,
            'df_list': ['Color-I', 'PPT_Control', ],
        }
        for i in range(1,10):
            profile['df_list'].append("Keypad%d" % i)
            profile['df_list'].append("Button%d" % i)
            profile['df_list'].append("Switch%d" % i)
            profile['df_list'].append("Knob%d" % i)
        result = csmapi.register('IoTtalk_Control_Panel', profile)
        if result:
            csmapi.push('IoTtalk_Control_Panel','__Ctl_I__',['SET_DF_STATUS_RSP',{'cmd_params':[]}])
            #for i in range(1,10):
            #    csmapi.push('IoTtalk_Control_Panel','Keypad%d' % i, [9])
            #    csmapi.push('IoTtalk_Control_Panel','Keypad%d' % i, [0])
            break
    except requests.exceptions.ConnectionError as e:
        print('requests.exceptions.ConnectionError:', e)
        print('retry after 3 second')
        time.sleep(3)
    except csmapi.CSMError as e:
        print('csmapi.CSMError:', e)
        print('retry after 3 second')
        time.sleep(3)

print('Remote_control registration is done.' )
