import time, DAN, requests, re, datetime  

alert_times = 1 # the number of alert times when the time is hitted.

ServerIP = '127.0.0.1' #Change to your IoTtalk IP or None for autoSearching
Reg_addr='Timer-I-Default' # if None, Reg_addr = MAC address

DAN.profile['dm_name']='Timer'
DAN.profile['df_list']=['Time1', 'Time2', 'Time3']
DAN.profile['d_name']= None # None for autoNaming
DAN.device_registration_with_retry(ServerIP, Reg_addr)

time_list=[0,0,0,0,0,0,0,0,0,0,0]
alerted = [0,0,0,0,0,0,0,0,0,0,0]
while True:
    try:
        for index in range(len(DAN.SelectedDF)):
            t_str = DAN.get_alias('Time'+str(index+1))
            try:
                time_list[index] =  datetime.datetime.strptime(t_str,'%H:%M')
                if time_list[index].hour == datetime.datetime.now().hour:
                    if time_list[index].minute == datetime.datetime.now().minute:
                        if alerted[index] <= alert_times:
                            DAN.push('Time'+str(index+1), 1)
                            print ('Timer'+str(index+1)+' Alert')
                            alerted[index] += 1
                    else: alerted[index] = 0
            except:
                print ('Time'+str(index+1) +': ' + t_str, ' is not correct time format, ignore.')

    except Exception as e:
        print(e)
        DAN.device_registration_with_retry(ServerIP, Reg_addr)

    time.sleep(1)
