from esm.df_func.helper_func import     read_data, write_data, csmapi,     json_loads, json_dumps
def run(*args):
    return sum(i*i for i in args)**0.5