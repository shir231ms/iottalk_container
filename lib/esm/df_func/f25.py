from esm.df_func.helper_func import     read_data, write_data, csmapi,     json_loads, json_dumps
def run(*args):
    acl = args [0]   
    gro = args [1]
    max_flag = 0
    max_number = acl[1]
    if(abs(acl[1])<abs(gro[1])) :
        max_flag = 1
        max_number = gro[1]
    if((max_flag == 0) and abs(max_number) > 1.5):
        if((acl[0] == 0) and (max_number> 0)):
            return 0
        elif(((acl[0] == 0) and (max_number < 0))):
            return 1
        elif(((acl[0] == 1) and (max_number > 0))):
            return 2
        elif(((acl[0] == 1) and (max_number < 0))):
            return 3
        elif(((acl[0] == 2) and (max_number > 0))):
            return 4
        elif(((acl[0] == 2) and (max_number < 0))):
            return 5
        else:
            return 18
    elif((max_flag == 1) and abs(max_number) > 1.5):
        if((gro[0] == 0) and (max_number> 0)):
            return 6
        elif(((gro[0] == 0) and (max_number < 0))):
            return 7
        elif(((gro[0] == 1) and (max_number > 0))):
            return 8
        elif(((gro[0] == 1) and (max_number < 0))):
            return 9
        elif(((gro[0] == 2) and (max_number > 0))):
            return 10
        elif(((gro[0] == 2) and (max_number < 0))):
            return 11
        else:
            return 18
    return 18