import ec_config
import db
import csmapi
import threading
import time

excluded_MAC_list = ['IoTtalk_Control_Panel','IoTtalk_Message','IoTtalk_SMS']

def get_MAC_addr_from_d_id(db, session, d_id):
    MAC_addr = (session.query(db.Device.mac_addr)
                .filter(db.Device.d_id == d_id)
                .first()[0]
               )
    return MAC_addr

def get_MAC_addr_from_do_id(db, session, do_id):
    MAC_list = (session.query(db.Device.mac_addr)
                .join(db.DeviceObject)
                .filter(db.Device.d_id == db.DeviceObject.d_id)
                .filter(db.DeviceObject.do_id == do_id)
                .first()
               )
    if MAC_list != None:
        MAC_addr = MAC_list[0]
    else: 
        MAC_addr = None
    return MAC_addr

def get_do_id_list_from_MAC_addr(db, session, do_id, MAC_addr):
    do_id_list = (session.query(db.DeviceObject.do_id)
                  .join(db.Device)
                  .filter(db.Device.d_id == db.DeviceObject.d_id)
                  .filter(db.Device.mac_addr == MAC_addr)
                )
    do_id_list = [column.do_id for column in do_id_list]
    do_id_list.append(do_id)
    return do_id_list

def get_all_MAC_addr(db, session, p_id):
    all_MAC_addr = (session.query(db.Device.mac_addr)
                    .join(db.DeviceObject)
                    .join(db.Project)
                    .filter(db.Device.d_id == db.DeviceObject.d_id)
                    .filter(db.DeviceObject.p_id == db.Project.p_id)
                    .filter(db.Project.p_id == p_id)
                   )
    return all_MAC_addr

control_channel_timestamp = None
def wait_for_SET_DF_STATUS_RSP(MAC_addr, ):
    global control_channel_timestamp
    Command = ['RESUME',{'cmd_params':[]}]
    for cycle in range (200):
        time.sleep(0.1)
        SET_DF_STATUS_RSP = csmapi.pull(MAC_addr, '__Ctl_I__')
        if  SET_DF_STATUS_RSP != []:
            if control_channel_timestamp == SET_DF_STATUS_RSP[0][0]: continue
            control_channel_timestamp = SET_DF_STATUS_RSP[0][0]
            if (SET_DF_STATUS_RSP[0][1][0] == 'SET_DF_STATUS_RSP'):
                csmapi.push(MAC_addr, '__Ctl_O__', Command) 
                print ('Got SET_DF_STATIS_RSP, then send RESUME command.')
                exit()
            else:
                print ('RSP is not correct:', SET_DF_STATUS_RSP[0][1][0]) 
        else:
            print ('SET_DF_STATUS_RSP is empty in cycle:',cycle)
    print ('Retry 10 times and failed to get SET_DF_STATUS, force send RESUME command.')
    csmapi.push(MAC_addr, '__Ctl_O__', Command)

def SET_DF_STATUS(db, session, do_id, d_id=None):
    
    if d_id == None:
        MAC_addr = get_MAC_addr_from_do_id(db, session, do_id)
    else:
        MAC_addr = get_MAC_addr_from_d_id(db, session, d_id)

    if MAC_addr != None:
        do_id_list = get_do_id_list_from_MAC_addr(db, session, do_id, MAC_addr)
        Device_profile = (csmapi.pull(MAC_addr, 'profile'))
        Real_df_list = Device_profile['df_list']

        if (Device_profile['dm_name'] == 'MorSensor'):
            DF_STATUS = ''
            x=0
            for x in range(len(Real_df_list)):
                DF_STATUS = DF_STATUS + '1'
        else:
            Selected_df_list = []

            '''
            #Only return the selected df_list from the last binded do_od
            Selected_df_list = (session.query(db.DeviceFeature.df_name)
                               .join(db.DFObject)
                               .filter(db.DeviceFeature.df_id == db.DFObject.df_id)
                               .filter(db.DFObject.do_id == do_id)
                               )        
            
            '''
            #Return the union of selected df_list from all binded do_id
            for do_id in do_id_list:
                Selected_df_list += (session.query(db.DeviceFeature.df_name)
                                .join(db.DFObject)
                                .filter(db.DeviceFeature.df_id == db.DFObject.df_id)
                                .filter(db.DFObject.do_id == do_id)
                                )
            

            DF_STATUS_list = ['0' for x in range(len(Real_df_list))]
            for column in Selected_df_list:
                try:
                    index = Real_df_list.index(column.df_name) #still need to deal with exception, otherwise it will cause crash!
                except ValueError:
                    print('Feature not found: "{}"'.format(column.df_name))
                else:
                    DF_STATUS_list[index] = '1'
            DF_STATUS = ''.join(DF_STATUS_list) 

        Command = ['SET_DF_STATUS',{'cmd_params':[DF_STATUS]}]
        print ('push to __Crl_O__:', Command)
        csmapi.push(MAC_addr, '__Ctl_O__', Command)
        
        prj_status = (session.query(db.Project.status)
                     .select_from(db.DeviceObject)
                     .join(db.Project)
                     .filter(db.DeviceObject.do_id == do_id)
                    ).first()
        if (prj_status != None): 
            if (prj_status[0] == 'off'):
                print ('Project status  == off')
                return 200
            else:
                print ('Project status == on')
                if (MAC_addr not in excluded_MAC_list):
                    threading.Thread(target=wait_for_SET_DF_STATUS_RSP, name='Thd-'+MAC_addr, args=(MAC_addr,)).start()
                    return 200
                else:
                    print ('Device cannot handle with RESUME command, do not send RESUME command.')
        return 200
    print ('MAC_addr is None.')
    return 400

def SUSPEND_device(db, session, do_id):
    MAC_addr = get_MAC_addr_from_do_id(db, session, do_id)
    if (MAC_addr not in excluded_MAC_list):
        Command = ['SUSPEND',{'cmd_params':[]}]
        csmapi.push(MAC_addr, '__Ctl_O__', Command)
        print ('SUSPEND_device:', MAC_addr)
    return 200

def SUSPEND(db, session, p_id):
    all_MAC_addr = get_all_MAC_addr(db, session, p_id)
    Command = ['SUSPEND',{'cmd_params':[]}]
    for column in all_MAC_addr:
        if (column.mac_addr not in excluded_MAC_list):
            csmapi.push(column.mac_addr, '__Ctl_O__', Command) 
    print ('SUSPEND all devices', p_id)
    return 200

def RESUME(db, session, p_id):
    all_MAC_addr = get_all_MAC_addr(db, session, p_id)
    Command = ['RESUME',{'cmd_params':[]}]
    for column in all_MAC_addr:
        if (column.mac_addr not in excluded_MAC_list):
            csmapi.push(column.mac_addr, '__Ctl_O__', Command)
    print ('RESUME all devices', p_id)
    return 200
