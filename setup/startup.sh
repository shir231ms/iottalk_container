#!/bin/sh

python3="python3"

cd $(dirname $0)
cd ../
ProjectPath=$(pwd)
ProjectName=$(echo $ProjectPath | tr "/" "\n" | tail -n 1)
echo "ProjectPath: $ProjectPath"
echo "ProjectName: $ProjectName"

cd $ProjectPath

export PYTHONPATH="$PYTHONPATH:$ProjectPath/lib"
LOG=$ProjectPath/log/startup.log
if [ ! -d $ProjectPath/log ]; then
    mkdir $ProjectPath/log
fi
if [ ! -d $ProjectPath/sqlite ]; then
    mkdir $ProjectPath/sqlite
fi
if [ ! -f $ProjectPath/sqlite/ec_db.db ]; then
    ./setup/reset_db.sh
fi

echo --------------------------------------- >> $LOG
date >> $LOG
echo --------------------------------------- >> $LOG

myIP=$(ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*'| grep -v '127.0.0.1')

screen -dmS $ProjectName >> $LOG 2>&1
add_to_screen() {
    TITLE=$1
    CMD=$2
    screen -S $ProjectName -X screen -t "$TITLE" bash -c \
        "\
        while [ 1 ]; do \
            $CMD; echo ========== restart ==========; sleep 1; \
        done"
}

# wait for screen.
while [ 1 ]; do
    ps aux | grep -v grep | grep SCREEN | grep $ProjectName > /dev/null 2>&1
    if [ $? -eq 0 ]; then
        break
    fi
    sleep 1
done

add_to_screen CSM "./bin/csm $python3" >> $LOG 2>&1
echo "Sleep 2 seconds for waitting CSM bootup."
sleep 2

add_to_screen SIM "./bin/ecsim $python3" >> $LOG 2>&1
add_to_screen CCM "./bin/ccm $python3" >> $LOG 2>&1
add_to_screen ESM "./bin/esm $python3" >> $LOG  2>&1
add_to_screen WEB "sudo $python3 ./da/web.py" >> $LOG 2>&1
add_to_screen Timer "sudo $python3 ./da/Timer/timer.py" >> $LOG 2>&1
add_to_screen MusicBox "cd ./da/MusicBox;npm install;nodejs Server.js $myIP:9999"  >> $LOG 2>&1
add_to_screen MorSocket "cd ./da/MorSocket-Server;npm install;nodejs MorSocketServer.js $myIP:9999"  >> $LOG 2>&1
add_to_screen Folder_O "$python3 ./da/Folder/Folder_O.py" >> $LOG 2>&1
#add_to_screen SMS "$python3 ./da/SMS/DAI.py" >> $LOG 2>&1
#add_to_screen Broadcast "$python3 ./bin/broadcast.py" >> $LOG 2>&1

sleep 3

sudo -HE env PYTHONPATH=$PYTHONPATH $python3 ./da/Remote_control/startup_panel.py
sudo -HE env PYTHONPATH=$PYTHONPATH $python3 ./da/Message/startup_msg.py

#echo "Waiting for CHT Pirius booting. (2 mintues.)"
#sleep 120
#add_to_screen CHT "nodejs ./da/IoTtalk-CHT-master/index.js" >> $LOG 2>&1

while true
do
	sleep 1
done

