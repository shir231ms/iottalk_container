# **I. Reference** -------------------------------------------------------------------------
## IMAGE cmd: 
<https://joshhu.gitbooks.io/dockercommands/content/DockerImages/CommandArgs.html>
<https://philipzheng.gitbooks.io/docker_practice/content/image/create.html>
## CONTAINER cmd: 
<https://joshhu.gitbooks.io/dockercommands/content/Containers/ContainersBasic.html>
## docker run argument: 
<https://jiajially.gitbooks.io/dockerguide/content/chapter_fastlearn/docker_run/index.html>
<https://jiajially.gitbooks.io/dockerguide/content/chapter_fastlearn/container_daily.html>
<https://docs.docker.com/v17.09/engine/userguide/networking/default_network/binding/>
# **II. Essential Command** -----------------------------------------------------------------
## List Docker CLI commands
```
sudo docker
```
or
```
sudo docker container --help
```
## Display Docker version and info
```
sudo docker --version
```
or
```
sudo docker version
```
or
```
sudo docker info
```
## get the image from Docker Hub or others
```
sudo docker pull [IMAGE_NAME]
```
## Execute Docker image and build a container in it.
```
sudo docker run [IMAGE_NAME] 
```
## List Docker images
```
sudo docker image ls
```
or
```
sudo docker images
```
## Search the container ID, List Docker containers (running, all, all in quiet mode)
```
sudo docker ps -a
```
or
```
sudo docker container ls
```
or
```
sudo docker container ls --all
```
or
```
sudo docker container ls -aq
```
## Search the running container
```
sudo docker ps
```
## Stop and delete the container running in the image, and then it is able to delete the image.	
### * Stop the container
```
sudo docker stop [CONTAINER_ID]
```
### * Remove the container
```
sudo docker rm [CONTAINER_ID]
```
### * Remove all containers
```
sudo docker rm $(sudo docker ps -a -q)
```
### * Remove the image
```
sudo docker rmi [IMAGE_NAME]
```
### * Remove all images
```
sudo docker rmi $(sudo docker images -q)
```
## Use Dockerfile to build Docker image, ensure that Dockerfile & the executed file (chmod 777) in the same directory
```
sudo docker build -t [IMAGE_NAME] .
```
Ex. *sudo docker build -t iottalk .*
## Run container through images
```
sudo docker run -di (--name [CONTAINER_NAME]) (--hostname [DOMAIN_NAME]) (-p [xxxx:xxxx] -p [xx:xx]...) [IMAGE_NAME] ([COMMAND])
```
Ex. *sudo docker run --name iottalk_ctnr --hostname wmnetsimplat.cs.nthu.edu.tw -di -p 80:80 -p 5566:5566 -p 7788:7788 -p 8000:8000 -p 9999:9999 iottalk* 
## Read the details of docker logs
```
sudo docker logs --details [CONTAINER_NAME/ID]
```
Ex. *sudo docker logs --details iottalk_ctnr*
# **III. Implementation** -------------------------------------------------------------------
## *** Non-Stop service nessecary conditions**
1. 開機自動執行常駐: Add "-d" argument in "docker run".
2. 執行一個不停止的服務: Non-stop program - "startup.sh" owns infinite loop.
3. 保持基本輸入或輸出的能力: Add "-t" or "-i" at the same time, such as "-dt" or "-di" or "-idt". 
## *** Domain name setting requirement:**
1. Edit /etc/hosts: type the specific domain name you want which seperated with space ' ' behind the IP address.
2. Add argument '--hostname [DOMAIN_NAME]' in "docker run" command.

